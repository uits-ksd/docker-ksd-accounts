# For a container with Kuali group and related users

# Start with our Kuali security image
FROM kuali/ksd-security
MAINTAINER U of A Kuali DevOps <katt-support@list.arizona.edu>

# Build variables
# Default is set to "dev"
ARG TARGET_ENV=dev

RUN echo 'Building image for:' $TARGET_ENV

# Copy in start script and make executable
COPY bin /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# Create kuali, kfs-services, kfs-writers, and kfs-writers-sup groups with specific gids
RUN groupadd -r -g 999 kuali && \
    groupadd -r -g 1001 kfs-services && \
    groupadd -r -g 1002 kfs-writers && \
    groupadd -r -g 1003 kfs-writers-sup

# Create kualiadm user with specific uid, put in groups, and set default shell
RUN useradd -s /bin/bash -u 998 -g 999 kbatch && \
    usermod -a -G kuali,kfs-services,kfs-writers,kfs-writers-sup kbatch

# Create pssys user with specific uid, put in groups, and set default shell
RUN useradd -s /bin/bash -u 997 -g 999 pssys && \
    usermod -a -G kuali,kfs-services,kfs-writers,kfs-writers-sup pssys

# Set up .ssh directories and copy in public SSH keys for each user
RUN mkdir -p /home/kbatch/.ssh
WORKDIR /home/kbatch/.ssh
COPY ssh/$TARGET_ENV/kbatch.pub .
RUN cat kbatch.pub > authorized_keys

RUN mkdir -p /home/pssys/.ssh
WORKDIR /home/pssys/.ssh
COPY ssh/$TARGET_ENV/pssys.pub .
RUN cat pssys.pub > authorized_keys

# Update user home directory permissions
RUN chown -R kbatch:kuali /home/kbatch/
RUN chmod 700 /home/kbatch/.ssh
RUN chown -R pssys:kuali /home/pssys/
RUN chmod 700 /home/pssys/.ssh

# Re-establish starting working directory
RUN pwd
WORKDIR /root
RUN echo 'Current working directory is back to the original value of: ' && pwd

ENTRYPOINT /usr/local/bin/establish_accounts.sh
