# University of Arizona Kuali Accounts Docker Container
---

### Description
This project defines an image that will set up service accounts for our Kuali Docker containers. 

### Building
A sample build command is: `docker build -t kuali/ksd-accounts:local --build-arg TARGET_ENV=local --force-rm .`

This command will build a kuali/ksd-accounts image for your local environment and tag it with the name local.

The purpose of the `TARGET_ENV` argument is to ensure the correct public SSH keys are included in the Docker image for the target deployment environment. Valid values for the `TARGET_ENV` are:

* local
* dev
* tst
* stg
* sup
* prd

The default value is dev if one is not provided.

To create an image for a specific environment, example commands are:

    docker pull kuali/ksd-security
    docker tag kuali/ksd-security kuali/ksd-security
    docker build -t kuali/ksd-accounts:dev7 --build-arg TARGET_ENV=dev --force-rm .

### Running
A sample run command (used for testing) is: `docker run -d --name=kuali-accounts --privileged=true kuali/ksd-accounts:local`

This command will run a container called kuali-accounts based on the kuali/ksd-accounts:local image.

### Technical Details

##### SSH Keys
We are storing the public SSH keys for the kbatch and pssys users in this repository. The corresponding private keys are stored on the Control-M servers.

If the SSH keys change, this project will need to be updated and a new Docker image will need to be created.

Heather also created an SSH key for local testing with the kbatch user or the pssys user. The public key for each is in ssh/local and they are identical. The private key is in stache in the "KFS Team Stuff" folder in the "kbatch Private SSH Key For Rhubarb Docker Container" entry.